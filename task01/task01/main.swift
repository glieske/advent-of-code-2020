import Foundation
import Common

let fms = FileManagerService()
let data = try fms.readFileByName(file: "input", type: "txt")

for number in data {
    let num = Int(number)
    let lookFor = 2020 - num!;
    if (data.contains(String(lookFor))) {
        let value = num! * lookFor
        print("Result Part 1: " + String(value))
        break
    }
}

func part2(data: [String]) -> Void {
    for number1 in data {
        let num1 = Int(number1)!
        let subdata1 = data.filter { $0 != number1 }
        for number1 in subdata1 {
            let num2 = Int(number1)!
            let subdata2 = subdata1.filter { $0 != number1 }
            for number2 in subdata2 {
                let num3 = Int(number2)!
                if (2020 - num3 - num2 - num1 == 0) {
                    let value = num1 * num2 * num3
                    print("Result Part 2: \(value)")
                    return
                }
            }
        }
    }
}
part2(data: data)


