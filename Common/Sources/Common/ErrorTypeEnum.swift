import Foundation

public enum ErrorTypeEnum: Error {
    case runtimeError(String)
}
