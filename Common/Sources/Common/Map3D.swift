import Foundation

public class Map3D {
    
    private var points: [Point3D] = []
    
    public init(){}
    
    public func addPoint(point: Point3D) -> Map3D {
        self.points.append(point)
        return self
    }
    
    public func removePoint(point: Point3D) -> Map3D {
        self.points = self.points.filter { $0 !== point }
        return self
    }
    
}
