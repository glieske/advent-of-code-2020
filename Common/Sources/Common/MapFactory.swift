import Foundation

public class MapFactory {
    public init () {}
    
    public static func mapValue2DMap(data: [String]) -> Map2D {
        var x: Int64 = 0
        var y: Int64 = 0
        let map: Map2D = Map2D()
        for item: String in data {
            let itemArray = Array(item)
            x = 0
            for char in itemArray {
                let pt = Point2D(x: x, y: y, value: String(char))
                map.addPoint(point: pt);
                x += 1
            }
            y += 1
        }
        return map
    }
}
