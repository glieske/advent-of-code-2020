//
//  File.swift
//  
//
//  Created by Grzegorz Lieske on 02/12/2020.
//

import Foundation

public class FileManagerService {
    private let fm: FileManager
    public init(){
        fm = FileManager()
    }
    
    public func readFileByName(file: String, type: String) throws -> [String] {
        if let path = Bundle.main.path(forResource: file, ofType: type) {
            do {
                let data = try String(contentsOfFile: path, encoding: .utf8)
                return data.components(separatedBy: .newlines)
            } catch {
                print(error)
                throw ErrorTypeEnum.runtimeError("read file error")
            }
        } else {
            throw ErrorTypeEnum.runtimeError("file error")
        }
    }
    
}
