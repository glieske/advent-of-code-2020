import Foundation

public class Point2D {
    var x: Int64
    var y: Int64
    var value: String?
    
    public init(x: Int64, y: Int64, value: String?) {
        self.x = x
        self.y = y
        self.value = value
    }
    
    public init(x: Int64, y: Int64) {
        self.x = x
        self.y = y
    }
    
    public func getX() -> Int64 {
        return self.x
    }
    
    public func getY() -> Int64 {
        return self.y
    }
    
    public func getValue() -> String? {
        return self.value
    }
    
    public func setX(_ v: Int64) -> Point2D {
        self.x = v
        return self
    }
    
    public func setY(_ v: Int64) -> Point2D {
        self.y = v
        return self
    }
    
    public func setValue(_ v: String?) -> Point2D {
        self.value = v
        return self
    }
    
    public func translateX(_ x: Int64) -> Point2D {
        self.x += x
        return self
    }
    
    public func translateY(_ y: Int64) -> Point2D {
        self.y += y
        return self
    }

    
    public func getValueForced() -> String {
        return self.value ?? ""
    }
    
    public func getDistance(point: Point2D) -> Float64 {
        let xDist: Int64 = point.x - self.x
        let yDist: Int64 = point.y - self.y
        return sqrt(pow(Double(xDist), 2) + pow(Double(yDist), 2))
    }
}
