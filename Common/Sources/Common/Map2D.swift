import Foundation

public class Map2D {
    
    private var points: [Point2D] = []
    
    public init(){}
    
    private init(points: [Point2D]) {
        self.points = points
    }
    
    public func addPoint(point: Point2D) -> Map2D {
        self.points.append(point)
        return self
    }
    
    public func removePoint(point: Point2D) -> Map2D {
        self.points = self.points.filter { $0 !== point }
        return self
    }
    
    public func filterByPointValue(value: String?) -> Map2D {
        let pts = self.points.filter { $0.getValue() == value }
        return Map2D(points: pts)
    }
    
    public func isSet(point: Point2D) -> Bool {
        return self.points.contains { $0.getX() == point.getX() && $0.getY() == point.getY() }
    }
    
    public func getMaxY() throws -> Int64 {
        var minY: Int64? = nil
        if self.points.count > 0 {
            for point in self.points {
                if let y = minY {
                    if y < point.getY() {
                        minY = point.getY()
                    }
                } else {
                    minY = point.getY()
                }
            }
        } else {
            throw ErrorTypeEnum.runtimeError("No entries")
        }
        return minY!
    }
    
    public func getMaxX() throws -> Int64 {
        var minX: Int64? = nil
        if self.points.count > 0 {
            for point in self.points {
                if let x = minX {
                    if x < point.getY() {
                        minX = point.getX()
                    }
                } else {
                    minX = point.getX()
                }
            }
        } else {
            throw ErrorTypeEnum.runtimeError("No entries")
        }
        return minX!
    }
}
