import Foundation

public class Point3D : Point2D {
    var z: Int64
    
    public init(x: Int64, y: Int64, z: Int64, value: String?) {
        self.z = z
        super.init(x: x, y: y, value: value)
    }
    
    public init(x: Int64, y: Int64, z: Int64) {
        self.z = z
        super.init(x: x, y: y)
    }
    
    public func getZ() -> Int64 {
        return self.z
    }
    
    public func getDistance(point: Point3D) -> Float64 {
        let xDist: Int64 = point.x - self.x
        let yDist: Int64 = point.y - self.y
        let zDist: Int64 = point.z - self.z
        
        return sqrt(pow(Double(xDist), 2) + pow(Double(yDist), 2) + pow(Double(zDist), 2))
    }
}
