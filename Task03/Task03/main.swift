import Foundation
import Common

let tree = "#"
let fms = FileManagerService()

let data = try fms.readFileByName(file: "input", type: "txt")
let map: Map2D = MapFactory.mapValue2DMap(data: data)

let mapX = try map.getMaxX()
let mapY = try map.getMaxY()
let treesMap = map.filterByPointValue(value: tree)

func traverseMap(travX: Int64, travY: Int64) -> Int {
    let cursor = Point2D(x: 0, y: 0)
    var x: Int64 = 0, y: Int64 = 0, counter: Int = 0
    while cursor.getY() <= mapY {
        if treesMap.isSet(point: cursor) {
            counter += 1
        }
        cursor
            .setX(cursor.getX() + travX)
            .setY(cursor.getY() + travY)
        if (cursor.getX() > mapX) {
            cursor.setX(cursor.getX() - mapX - 1)
        }
    }
    return counter
}

print ("Result part1: \(traverseMap(travX: 3, travY: 1))")
let part2: Int = traverseMap(travX: 1, travY: 1) * traverseMap(travX: 3, travY: 1) * traverseMap(travX: 5, travY: 1) * traverseMap(travX: 7, travY: 1) * traverseMap(travX: 1, travY: 2);
print ("Result part2: \(part2)")

