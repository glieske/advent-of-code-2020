import Foundation
import Common

let fms = FileManagerService()
let data = try fms.readFileByName(file: "input", type: "txt")
var maxSeat = 0;
func intFloorDiv2(from: Int, to: Int) -> Int {
    return Int(floor((Double(from) + Double(to)) / 2))
}
func intRoundDiv2(from: Int, to: Int) -> Int {
    return Int(round((Double(from) + Double(to)) / 2))
}
var seats: [Int] = []
for datum in data {
    var from = 0, to = 127, rowFrom = 0, rowTo = 7
    let arr = Array(datum)
    for char in arr {
        let strChar = String(char)
        if (strChar == "F") {
            to = intFloorDiv2(from: from, to: to)
        } else if (strChar == "B") {
            from = intRoundDiv2(from: from, to: to)
        } else if (strChar == "R") {
            rowFrom = intRoundDiv2(from: rowFrom, to: rowTo)
        } else if (strChar == "L") {
            rowTo = intFloorDiv2(from: rowFrom, to: rowTo)
        }
    }
    let seat = to * 8 + rowTo
    seats.append(seat)
    maxSeat = maxSeat < seat ? seat : maxSeat
    print("Seat: row: \(from); column: \(rowTo); seat: \(seat)")
}
print ("Max seat: \(maxSeat)")
seats.sort()
for st in seats {
    let prev = seats.prev(st)
    let next = seats.next(st)
    if (prev == nil || next == nil) {
        continue;
    }
    if (prev! + 1 == st && st + 2 == next) {
        print("My seat: \(st + 1)")
    }

}
