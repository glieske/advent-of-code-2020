import Foundation
import Common

let fms = FileManagerService()
let data = try fms.readFileByName(file: "input", type: "txt")
var validPasswordsPart1: Int = 0
var validPasswordsPart2: Int = 0

for row: String in data {
    let parts: [String] = row.components(separatedBy: .whitespaces)
    if (parts.count != 3) {
        throw ErrorTypeEnum.runtimeError("Parsing error")
    } else {
        let part1 = parts[0]
        let part2 = parts[1]
        let part3 = parts[2]
        let amountFromParts = part1.components(separatedBy: "-")
        let from = Int(amountFromParts[0])!
        let to = Int(amountFromParts[1])!
        let targetChar = Array(part2)[0]
        /* Part 1 */
        var localCounter: Int = 0
        for char in part3 {
            if (char == targetChar) {
                localCounter += 1
            }
        }
        if (localCounter >= from && localCounter <= to) {
            validPasswordsPart1 += 1
        }
        /* End Part 1 */
        /* Part 2 */
        let arrPass = Array(part3)
        if (arrPass[from - 1] != arrPass[to - 1]
                && (arrPass[from - 1] == targetChar || arrPass[to - 1] == targetChar)
        ) {
            validPasswordsPart2 += 1
        }
        /* End Part 2 */
    }
}

print("Result Part 1: " + String(validPasswordsPart1))
print("Result Part 2: " + String(validPasswordsPart2))

