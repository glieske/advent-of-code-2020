import Foundation
import Common

let fms = FileManagerService()
let data = try fms.readFileByName(file: "input", type: "txt")

let requiredPassportFields: [String] = ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"]

var validPassports: Int = 0
var validPassportsPart2: Int = 0
var foundKeys: [String] = []
var foundCorrectKeys: [String] = []

func validateEntry(key: String, value: String) -> Bool {
    switch key {
    case "byr":
        let intVal = Int(value)
        if intVal != nil {
            return intVal! >= 1920 && intVal! <= 2002
        }
        break
    case "iyr":
        let intVal = Int(value)
        if intVal != nil {
            return intVal! >= 2010 && intVal! <= 2020
        }
        break
    case "eyr":
        let intVal = Int(value)
        if intVal != nil {
            return intVal! >= 2020 && intVal! <= 2030
        }
        break;
    case "hgt":
        switch String(value.suffix(2)) {
        case "cm":
            let intVal = Int(value.prefix(value.count - 2))
            if intVal != nil {
                return intVal! >= 150 && intVal! <= 193
            }
            break;
        case "in":
            let intVal = Int(value.prefix(value.count - 2))
            if intVal != nil {
                return intVal! >= 59 && intVal! <= 76
            }
            break;
        default:
            return false
        }
        break
    case "hcl":
        return value ~= "^#([a-z0-9]){6}$"
    case "ecl":
        return ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"].contains(value)
    case "pid":
        return value ~= "^[0-9]{9}$"
    case "cid":
        return true
    default:
        return false
    }
    return false
}

for datum in data {
    if datum != "" {
        let keyPairs = datum.components(separatedBy: " ")
        for keyPair in keyPairs {
            let dt = keyPair.components(separatedBy: ":")
            let key = dt.first!
            let value = dt.last!
            foundKeys.append(key)
            if (validateEntry(key: key, value: value)) {
                foundCorrectKeys.append(key)
            }
        }
    }
    if datum == "" || datum == data.last {
        var fieldsOk: Bool = true
        var fieldsWithValidationOk: Bool = true
        for requiredField in requiredPassportFields {
            if (!foundKeys.contains(requiredField)) {
                fieldsOk = false
            }
            if (!foundCorrectKeys.contains(requiredField)) {
                fieldsWithValidationOk = false
            }
        }
        if (fieldsOk) {
            validPassports += 1
        }
        if (fieldsWithValidationOk) {
            validPassportsPart2 += 1
        }
        foundKeys = []
        foundCorrectKeys = []
    }
}

print("Result part1: \(validPassports)")
print("Result part2: \(validPassportsPart2)")
