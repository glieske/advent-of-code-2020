import Foundation
import Common

let fms = FileManagerService()
let data = try fms.readFileByName(file: "input", type: "txt")

var subBuffer: [String] = []
var part2Buffer: [String] = []
var countPart1: Int = 0
var countPart2: Int = 0

for (index, datum) in data.enumerated() {
    let chars = Array(datum)
    for char in chars {
        let strChar = String(char)
        subBuffer.append(strChar)
    }
    if (datum != "") {
        part2Buffer.append(datum)
    }
    if (data.indices.last == index || datum == "") {
        countPart1 += subBuffer.unique.count
        var intersection: Set<Character> = []
        while (part2Buffer.count > 0) {
            let person = part2Buffer.first!
            part2Buffer = Array(part2Buffer.dropFirst())
            let next = part2Buffer.first
            if (intersection.count == 0) {
                intersection = Set(Array(person))
            }
            if (next != nil) {
                let nextSet = Set(Array(next!))
                intersection = intersection.intersection(nextSet)
                if (intersection.count == 0) { break }
            }
        }
        countPart2 += intersection.count
        part2Buffer = []
        subBuffer = []
    }
}

print("Part1: \(countPart1)")
print("Part2: \(countPart2)")
